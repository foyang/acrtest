package com.hz.acrKasseService.security.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN,
    ROLE_PRESIDENT,
    ROLE_COACH,
    ROLE_DS,
    ROLE_HELP,
    ROLE_SUPER_ADMIN
}
