package com.hz.acrKasseService.security.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
