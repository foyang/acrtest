package com.hz.acrKasseService.security.resetPassword.controller;


import com.hz.acrKasseService.security.controller.AuthRestAPIs;
import com.hz.acrKasseService.mail.NotificationService;
import com.hz.acrKasseService.security.model.User;
import com.hz.acrKasseService.security.repository.UserRepository;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import com.hz.acrKasseService.security.resetPassword.model.PasswordForgotDto;
import com.hz.acrKasseService.security.resetPassword.model.PasswordResetToken;
import com.hz.acrKasseService.security.resetPassword.repository.PasswordResetTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.UUID;

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/forgot-password")
public class PasswordForgotControlle {

    Logger logger = LoggerFactory.getLogger(AuthRestAPIs.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private
    PasswordResetTokenRepository tokenRepository;

    @Autowired
    private NotificationService notificationService;

    @ModelAttribute("forgotPasswordForm")
    public PasswordForgotDto forgotPasswordDto() {
        return new PasswordForgotDto();
    }

    @GetMapping
    public String displayForgotPasswordPage() {
        return "forgot-password";
    }

    @PostMapping("/request")
    public ResponseEntity<?> processForgotPasswordForm(@Valid @RequestBody PasswordForgotDto passwordForgotDto, HttpServletRequest request) {

/*
        if (result.hasErrors()) {
            return "forgot-password";
        }
*/
        User user = userRepository.findByEmail(passwordForgotDto.getEmail());
        if (user == null) {
            return new ResponseEntity<>(new ResponseMessage("email null We could not find an account for that e-mail address."),
                    HttpStatus.BAD_REQUEST);
            /*result.rejectValue("email", null, "We could not find an account for that e-mail address.");
            return "forgot-password";*/
        }

        PasswordResetToken token = new PasswordResetToken();
        token.setToken(UUID.randomUUID().toString());
        token.setUser(user);
        token.setExpiryDate(30);
        tokenRepository.save(token);

            /*Mail mail = new Mail();
            mail.setFrom("no-reply@memorynotfound.com");
            mail.setTo(user.getEmail());
            mail.setSubject("Password reset request");

            Map<String, Object> model = new HashMap<>();
            model.put("token", token);
            model.put("user", user);
            model.put("signature", "https://memorynotfound.com");
            model.put("resetUrl", url + "/reset-password?token=" + token.getToken());
            mail.setModel(model);
            emailService.sendEmail(mail);*/

        //String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        String url = passwordForgotDto.getFrontResetPasswordUrl();
        String urlToSent = url + "?token=" + token.getToken();
        notificationService.sendResetPasswordToken(user.getEmail(), urlToSent);

        return new ResponseEntity<>(new ResponseMessage("reset password was sent to this email adresse: "+ passwordForgotDto.getEmail()+" successfully!"), HttpStatus.OK);
    }
}
