package com.hz.acrKasseService.security.resetPassword.controller;



import com.hz.acrKasseService.security.model.User;
import com.hz.acrKasseService.security.repository.UserRepository;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import com.hz.acrKasseService.security.resetPassword.model.PasswordResetDto;
import com.hz.acrKasseService.security.resetPassword.model.PasswordResetToken;
import com.hz.acrKasseService.security.resetPassword.repository.PasswordResetTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/reset-password")
public class PasswordResetController {

    @Autowired
    private UserRepository userRepository;


    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private PasswordResetTokenRepository tokenRepository;


    @ModelAttribute("passwordResetForm")
    public PasswordResetDto passwordReset() {
        return new PasswordResetDto();
    }

    @GetMapping
    public String displayResetPasswordPage(@RequestParam(required = false) String token,
                                           Model model) {

        PasswordResetToken resetToken = tokenRepository.findByToken(token);
        if (resetToken == null){
            model.addAttribute("error", "Could not find password reset token.");
        } else if (resetToken.isExpired()){
            model.addAttribute("error", "Token has expired, please request a new password reset.");
        } else {
            model.addAttribute("token", resetToken.getToken());
        }

        return "reset-password";
    }

    @PostMapping("/reset")
    @Transactional
    public ResponseEntity<?> handlePasswordReset(@Valid @RequestBody PasswordResetDto form) {

/*
        if (result.hasErrors()){
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".passwordResetForm", result);
            redirectAttributes.addFlashAttribute("passwordResetForm", form);
            return "redirect:/reset-password?token=" + form.getToken();
        }
*/

        PasswordResetToken token = tokenRepository.findByToken(form.getToken());
        User user = token.getUser();
        String updatedPassword = passwordEncoder.encode(form.getPassword());
        userRepository.updatePassword(updatedPassword, user.getId());
        tokenRepository.delete(token);
        return new ResponseEntity<>(new ResponseMessage("Password was reset successfully!"), HttpStatus.OK);
    }


}
