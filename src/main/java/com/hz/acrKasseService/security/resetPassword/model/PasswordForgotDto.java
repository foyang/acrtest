package com.hz.acrKasseService.security.resetPassword.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PasswordForgotDto {

    @NotEmpty
    @Size(max = 50)
    @javax.validation.constraints.Email
    private String email;

    @NotNull
    private String frontResetPasswordUrl;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFrontResetPasswordUrl() {
        return frontResetPasswordUrl;
    }

    public void setFrontResetPasswordUrl(String frontResetPasswordUrl) {
        this.frontResetPasswordUrl = frontResetPasswordUrl;
    }
}
