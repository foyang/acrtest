package com.hz.acrKasseService.security.resetPassword.service;


import com.hz.acrKasseService.security.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(User registration);

    void updatePassword(String password, Long userId);
}
