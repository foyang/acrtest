package com.hz.acrKasseService.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hz.acrKasseService.cashbox.dao.AcrCahsCodeDao;
import com.hz.acrKasseService.cashbox.dao.AcrSessionDao;
import com.hz.acrKasseService.cashbox.dao.InOutDao;
import com.hz.acrKasseService.cashbox.model.AcrCashCode;
import com.hz.acrKasseService.cashbox.model.AcrSession;
import com.hz.acrKasseService.cashbox.model.GeneralAssembly;
import com.hz.acrKasseService.cashbox.model.InOutM;
import com.hz.acrKasseService.mail.EmailService;
import com.hz.acrKasseService.mail.Mail;
import com.hz.acrKasseService.members.MemberRegistration;
import com.hz.acrKasseService.members.MemberRegistrationDao;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ServiceHelper {



    Logger logger = LoggerFactory.getLogger(this.getClass());


    private Date currentDate = new Date(System.currentTimeMillis());
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    public AcrSession getCurrentSeesion(AcrSessionDao acrSessionDao) throws ParseException {
        List<AcrSession> acrSessionList = acrSessionDao.findAll();

        for (AcrSession acrSession:acrSessionList){
            Date sessionEndDate = sdf.parse(acrSession.getAcrSessionEndDate());

            if (sessionEndDate.after(currentDate)){
                logger.info("######: "+acrSession);
                return acrSession;
            }
        }
        return null;
    }

    public GeneralAssembly getCurrentAg(AcrSessionDao acrSessionDao) throws ParseException {
        logger.info("DEDANNNN: ");
        AcrSession currentSeesion = getCurrentSeesion(acrSessionDao);
        logger.info("DEMANDE: CurrentSession: "+currentSeesion.getId());
        Set<GeneralAssembly> generalAssemblies = currentSeesion.getGeneralAssemblies();

        List<GeneralAssembly> generalAssemblyList = new ArrayList<>();
        generalAssemblyList.addAll(generalAssemblies);
        logger.info("DEDANA: List Size: "+generalAssemblyList.size());

        for (GeneralAssembly generalAssembly:generalAssemblyList){
            Date generalAssemblyEndDate = sdf.parse(generalAssembly.getAgEndDate());
            logger.info("####DANS LE FOR");
            logger.info("####CURRENT: "+currentDate);
            logger.info("####CURRENT: "+generalAssemblyEndDate);
            if (generalAssemblyEndDate.after(currentDate)){
                logger.info("GENAL: "+generalAssembly);
                return  generalAssembly;
            }
        }
        return null;
    }


    public List<AcrCashCode> getAcrCashCodes(AcrCahsCodeDao acrCahsCodeDao) {
        return acrCahsCodeDao.findAll();
    }


    public ResponseEntity<ResponseMessage> memberRegistrationProzess(MemberRegistration memberRegistration,
                                                                     AcrSession acrSession,
                                                                     AcrCahsCodeDao acrCahsCodeDao,
                                                                     AcrSessionDao acrSessionDao,
                                                                     InOutDao inOutDao,
                                                                     MemberRegistrationDao memberRegistrationDao,
                                                                     EmailService emailService) throws ParseException, IOException, MessagingException {

        Map<String, Object> model = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String currentDateString = sdf.format(new Date());

        memberRegistration.setRegistrationDate(currentDateString);
        memberRegistration.setAcrSession(acrSession);
        Mail mail = new Mail();
        mail.setFrom("kontakt.bookme@gofastonline.com");
        mail.setTo(memberRegistration.getEmail());
        mail.setSubject("Registration Successful");

        model.put("location", "Germany");
        model.put("signature", "https://gofastonline.com");

        mail.setModel(model);

        ObjectMapper oMapper = new ObjectMapper();

        Map<String, Object> mapMembere = oMapper.convertValue(memberRegistration, Map.class);

        AcrCashCode acrCashCode = null;

        for (AcrCashCode acrCashCode1 : getAcrCashCodes(acrCahsCodeDao)) {
            if (mapMembere.get("profesion").equals("Veterang")){
                mapMembere.put("profesion","Mitglied veteran");
            }

            if (acrCashCode1.getName().equals(mapMembere.get("profesion"))) {
                acrCashCode = acrCashCode1;
            }
        }


        if (acrCashCode != null) {

            InOutM inOutM = new InOutM();
            if (mapMembere.get("paymentMethod").equals("Paypal")) {
                inOutM.setIscheck(true);
            }else {
                inOutM.setIscheck(false);
            }
            logger.info(":::::::: " + mapMembere.get("paymentMethod").equals("Paypal"));
            logger.info("mmmkmmmk: "+inOutM.getIscheck());
            inOutM.setUser(null);
            inOutM.setIntryDate(mapMembere.get("registrationDate").toString());
            inOutM.setAcrCashCode(acrCashCode);
            inOutM.setAcrSession(acrSession);
            inOutM.setAmount((Integer) mapMembere.get("amount"));
            if (mapMembere.get("message") != null){
                inOutM.setComment(mapMembere.get("message").toString());
            }
            inOutM.setPaymentReason("Member Registration");
            inOutM.setGeneralAssembly(getCurrentAg(acrSessionDao));

            logger.info("#########: "+getCurrentAg(acrSessionDao).getId());
            inOutM.setIntryArt("Input");
            inOutM.setSender(mapMembere.get("firstName").toString() + " " + mapMembere.get("lastName").toString());
            inOutM.setManager("Service System");
            InOutM save = inOutDao.save(inOutM);
            MemberRegistration save1 = memberRegistrationDao.save(memberRegistration);

            if (save != null && save1 != null) {
                emailService.sendSimpleMessage(mail, mapMembere);
            }
        }else {
            return new ResponseEntity<>(new ResponseMessage("Code Not Found"),
                    HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new ResponseMessage("Your registration has been done successfully"), HttpStatus.CREATED);
    }

}
