package com.hz.acrKasseService.archive.controller;


import com.hz.acrKasseService.cashbox.dao.AcrSessionDao;
import com.hz.acrKasseService.cashbox.model.AcrSession;
import com.hz.acrKasseService.helper.ServiceHelper;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(description = "Create a Session Archive")
@RequestMapping(value = "archive")
public class SessionController {

    private Date currentDate = new Date(System.currentTimeMillis());
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");


     private ServiceHelper serviceHelper = new ServiceHelper();

    @Autowired
    private AcrSessionDao acrSessionDao;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("session")
    public List<AcrSession> getSessionArchive() throws ParseException {


        serviceHelper.getCurrentSeesion(acrSessionDao);

        AcrSession currentSeesion = new AcrSession();
        logger.info("#####: "+serviceHelper.getCurrentSeesion(acrSessionDao));

        List<AcrSession> acrSessionList = acrSessionDao.findAll();
        List<AcrSession> finalAcrSessionList = new ArrayList<>();

        for (AcrSession acrSession : acrSessionList){
            if (acrSession != currentSeesion){
                finalAcrSessionList.add(acrSession);
            }
        }

        return finalAcrSessionList;
    }
}
