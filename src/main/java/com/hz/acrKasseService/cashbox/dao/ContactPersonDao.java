package com.hz.acrKasseService.cashbox.dao;


import com.hz.acrKasseService.cashbox.model.ContactPerson;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactPersonDao  extends JpaRepository<ContactPerson,Integer> {


    ContactPerson findByName(String name);
}
