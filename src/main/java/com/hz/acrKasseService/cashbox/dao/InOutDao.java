package com.hz.acrKasseService.cashbox.dao;

import com.hz.acrKasseService.cashbox.model.InOutM;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InOutDao extends JpaRepository<InOutM,Integer> {
}
