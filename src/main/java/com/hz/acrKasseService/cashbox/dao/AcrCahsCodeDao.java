package com.hz.acrKasseService.cashbox.dao;

import com.hz.acrKasseService.cashbox.model.AcrCashCode;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public interface AcrCahsCodeDao extends JpaRepository<AcrCashCode,Integer> {
    AcrCashCode findByName(String name);
}
