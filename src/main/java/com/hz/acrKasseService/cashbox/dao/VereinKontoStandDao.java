package com.hz.acrKasseService.cashbox.dao;

import com.hz.acrKasseService.cashbox.model.VereinKontoStandM;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VereinKontoStandDao extends JpaRepository<VereinKontoStandM,Integer> {

}
