package com.hz.acrKasseService.cashbox.dao;

import com.hz.acrKasseService.cashbox.model.AcrCashCode;
import com.hz.acrKasseService.cashbox.model.Sympathisant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SympathisantDao extends JpaRepository<Sympathisant,Integer> {
    Sympathisant findByName(String name);
}
