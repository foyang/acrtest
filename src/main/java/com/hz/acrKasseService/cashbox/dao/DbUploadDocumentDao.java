package com.hz.acrKasseService.cashbox.dao;

import com.hz.acrKasseService.cashbox.model.DbUploadDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DbUploadDocumentDao extends JpaRepository<DbUploadDocument, String> {
}
