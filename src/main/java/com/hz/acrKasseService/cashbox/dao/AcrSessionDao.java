package com.hz.acrKasseService.cashbox.dao;

import com.hz.acrKasseService.cashbox.model.AcrSession;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcrSessionDao extends JpaRepository<AcrSession,Integer> {

    AcrSession findByAcrSessionNumber(int id);
}

