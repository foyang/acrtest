package com.hz.acrKasseService.cashbox.dao;

import com.hz.acrKasseService.cashbox.model.GeneralAssembly;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GeneralAssemblyDao extends JpaRepository<GeneralAssembly,Integer> {

    GeneralAssembly findBygeneralAssemblyNummer(int id);
}
