package com.hz.acrKasseService.cashbox.dao;

import com.hz.acrKasseService.cashbox.model.AcrCashCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcrCashCodeDao extends JpaRepository<AcrCashCode,Integer> {
}
