package com.hz.acrKasseService.cashbox.controller;

import com.hz.acrKasseService.cashbox.dao.AcrSessionDao;
import com.hz.acrKasseService.cashbox.dao.SympathisantDao;
import com.hz.acrKasseService.cashbox.model.AcrSession;
import com.hz.acrKasseService.cashbox.model.Sympathisant;
import com.hz.acrKasseService.exception.RunTimeException;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(description = "this Service allows you to add new Sympathisan.")
@RequestMapping(value = "sympathisan")
@PreAuthorize("hasRole('SUPER_ADMIN') or hasRole('PRESIDENT') or hasRole('ADMIN')")
public class SympathisantController {

    @Autowired
    private SympathisantDao sympathisantDao;

    @Autowired
    private AcrSessionDao acrSessionDao;

    @ApiOperation(value = "Add new Sympathisan")
    @PostMapping(value = "session/{session_id}/add")
    public ResponseEntity<?> addSympathisan(@Valid @RequestBody Sympathisant sympat,@PathVariable("session_id") int session_id) {

        Sympathisant sympathisant = sympathisantDao.findByName(sympat.getName());

        if (sympat.getName() == null){
            return new ResponseEntity<>(new ResponseMessage("Sympathisan name can not be null."),
                    HttpStatus.NOT_ACCEPTABLE);
        }
        AcrSession acrSession = acrSessionDao.findById(session_id)
                .orElseThrow(()-> new RunTimeException("The ACR-Session with id: "+session_id+" was not found"));
        if (sympathisant == null){
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String currentDateString = sdf.format(new Date());
            sympat.setIntryDate(currentDateString);
            sympat.setAcrSession(acrSession);
            sympathisantDao.save(sympat);
        }else {
            return new ResponseEntity<>(new ResponseMessage("Sympathisan with name "+sympat.getName()+" already exist"),
                    HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity<>(new ResponseMessage("The new Sympathisan has been successfully registered"), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Delete Sympathisan")
    @DeleteMapping(value = "delete/{id}")
    public ResponseEntity<?> deleteSympathisan(@Valid @PathVariable("id") int id){
        Sympathisant sympathisant = sympathisantDao.findById(id)
                .orElseThrow(()-> new IllegalArgumentException("The Sympathisan with id: "+id+" was not found: "));
        sympathisantDao.delete(sympathisant);
        return new ResponseEntity<>(new ResponseMessage("The Sympathisan with id: "+id+" was deleted"), HttpStatus.OK);
    }

    @ApiOperation(value = "Update Sympathisan")
    @PutMapping("update/{id}")
    public ResponseEntity<?> UpdateSympathisan(@Valid @RequestBody Sympathisant sympathisant, @PathVariable("id") int id){
        updateSympathisan(id, sympathisant.getName(), sympathisant.getIntryDate(), sympathisant.getAcrSession());
        return new ResponseEntity<>(new ResponseMessage("The Sympathisan with id: "+id+" was update successfully"), HttpStatus.OK);
    }

    public Sympathisant updateSympathisan(int id, String name, String intryDate, AcrSession acrSession){
        Sympathisant sympathisant = sympathisantDao.findById(id)
                .orElseThrow(() -> new RuntimeException("The Sympathisan with id: "+id+" was not found"));
        sympathisant.setName(name);
        if (acrSession != null){
            sympathisant.setAcrSession(acrSession);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String currentDateString = sdf.format(new Date());
        sympathisant.setIntryDate(currentDateString);

        sympathisantDao.save(sympathisant);
        return sympathisant;
    }

    @ApiOperation(value = "Get all Sympathisan")
    @GetMapping("list")
    public List<Sympathisant> getAllCodes(){
        List<Sympathisant> listSympathisan = sympathisantDao.findAll();
        return listSympathisan;
    }
}
