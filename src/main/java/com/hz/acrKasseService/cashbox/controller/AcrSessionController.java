package com.hz.acrKasseService.cashbox.controller;

import com.hz.acrKasseService.cashbox.dao.AcrSessionDao;
import com.hz.acrKasseService.cashbox.model.AcrSession;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "inout/session")
@Api(description = "this Service allows you to add a new ACR-Session.")
public class AcrSessionController {

    @Autowired
    private AcrSessionDao acrSessionDao;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private Date currentDate = new Date(System.currentTimeMillis());
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    @ApiOperation(value = "add new ACR-Session -> Date Format: 'dd-MM-yyyy' ")
    @PostMapping("/add")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    public ResponseEntity<?> addAcrSession(@Valid  @RequestBody AcrSession acrSession) throws ParseException {
        Date date1 = sdf.parse(acrSession.getArcSessionStartDate());
        Date date2 = sdf.parse(acrSession.getAcrSessionEndDate());

        String currentDateString = sdf.format(currentDate);
        Date currentDateFormated = sdf.parse(currentDateString);

        AcrSession acrSessionNumber = acrSessionDao.findByAcrSessionNumber(acrSession.getAcrSessionNumber());

        if (acrSessionNumber == null){
            if (!date1.after(date2) && !date1.before(currentDateFormated) && !date2.before(currentDateFormated)){
                acrSessionDao.save(acrSession);
            }else {
                return new ResponseEntity<>(new ResponseMessage("The date of the session does not match or is in the past"), HttpStatus.NOT_ACCEPTABLE);
            }
        }else {
            return new ResponseEntity<>(new ResponseMessage("The Session with number "+acrSession.getAcrSessionNumber()+" already exist"), HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity<>(new ResponseMessage("The new Session has been successfully registered"), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update a Session")
    @PutMapping(value = "/update/{id}", consumes = "application/json", produces = "application/json")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    public ResponseEntity<?> updateAcrSession(@Valid @RequestBody AcrSession acrSession, @PathVariable("id") int id){
        update(id,acrSession.getAcrSessionNumber(),acrSession.getArcSessionStartDate(),acrSession.getAcrSessionEndDate());
        return new ResponseEntity<>(new ResponseMessage("The Session with Number: "+acrSession.getAcrSessionNumber()+" was update successfully"), HttpStatus.OK);
    }

    public AcrSession update(int id, int acrSessionNumber, String arcSessionStartDate, String acrSessionEndDate){
        AcrSession acrSession = acrSessionDao.findById(id)
                .orElseThrow(() -> new RuntimeException("The ACR-Session with Number: "+acrSessionNumber+" was not found"));
        acrSession.setAcrSessionNumber(acrSessionNumber);
        acrSession.setArcSessionStartDate(arcSessionStartDate);
        acrSession.setAcrSessionEndDate(acrSessionEndDate);
        acrSessionDao.save(acrSession);
        return acrSession;
    }

    @ApiOperation(value = "Delete ACR-Session")
    @DeleteMapping(value = "/delete/{id}")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    public ResponseEntity<?> delete(@Valid @PathVariable("id") int id){
        AcrSession acrSession = acrSessionDao.findById(id)
                .orElseThrow(()-> new IllegalArgumentException("The ACR Session with id: "+id+" was not found: "));
        acrSessionDao.delete(acrSession);
        return new ResponseEntity<>(new ResponseMessage("The ACR Session with id: "+id+" was deleted"), HttpStatus.OK);
    }

    @ApiOperation(value = "Get Current Session")
    @GetMapping(value = "/currentsession")
    public AcrSession getCurrentSessions() throws ParseException {
        List<AcrSession> allSession = acrSessionDao.findAll();
        AcrSession actulSession = new AcrSession();
        for (AcrSession acrSession:allSession){
            Date date2 = sdf.parse(acrSession.getAcrSessionEndDate());
            String currentDateString = sdf.format(currentDate);
            Date currentDateFormated = sdf.parse(currentDateString);
            if (date2.after(currentDateFormated)){
                actulSession = acrSession;
            }
        }
        return actulSession;
    }


    @ApiOperation(value = "Get All Session")
    @GetMapping(value = "/list")
    public List<AcrSession> getListSessions(){
        List<AcrSession> allSession = acrSessionDao.findAll();
        return allSession;
    }
}
