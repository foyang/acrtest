package com.hz.acrKasseService.cashbox.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hz.acrKasseService.cashbox.dao.*;
import com.hz.acrKasseService.cashbox.model.*;
import com.hz.acrKasseService.exception.RunTimeException;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import com.hz.acrKasseService.security.model.Role;
import com.hz.acrKasseService.security.model.User;
import com.hz.acrKasseService.security.repository.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@CrossOrigin(origins = "*", maxAge = 3600)
@Api(description = "this service allows you to record the various entries of the association.")
@RestController
@RequestMapping(value = "inout")
@PreAuthorize("hasRole('SUPER_ADMIN') or hasRole('PRESIDENT') or hasRole('ADMIN') or hasRole('HELP')")
public class InOutController {
    @Autowired
    private InOutDao inOutDao;

    @Autowired
    private AcrSessionDao acrSessionDao;

    @Autowired
    private GeneralAssemblyDao generalAssemblyDao;

    @Autowired
    private AcrCahsCodeDao acrCahsCodeDao;

    @Autowired
    private com.hz.acrKasseService.cashbox.service.DBFileStorageService DBFileStorageService;

    @Autowired
    private UserRepository userRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @ApiOperation("Add In/Out")
    @PostMapping("username/{username}/session/{session_id}/ag/{ag_id}/code/{code_id}/add")
    public ResponseEntity<?> addInOut(
            @PathVariable("session_id") int session_id,
            @PathVariable("ag_id") int ag_id,
            @PathVariable("code_id") int code_id,
            @RequestParam(value = "file",required = false) MultipartFile file,
            @RequestParam("inout") String inoutString,
            @PathVariable("username") String userName
    ) throws Exception {

        InOutM inOut = new ObjectMapper().readValue(inoutString, InOutM.class);

        DbUploadDocument dbUploadDocument = DBFileStorageService.storeFile(file);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String currentDateString = sdf.format(new Date());

        inOut.setIntryDate(currentDateString);

        AcrSession acrSession = acrSessionDao.findById(session_id)
                .orElseThrow(() -> new RunTimeException("The ACR-Session with id: " + session_id + " was not found"));

        GeneralAssembly generalAssembly = generalAssemblyDao.findById(ag_id)
                .orElseThrow(() -> new RunTimeException("The General Assembly with id: " + ag_id + " was not found"));

        AcrCashCode acrCashCode1 = acrCahsCodeDao.findById(code_id)
                .orElseThrow(() -> new RunTimeException("The Code with id: " + code_id + " was not found"));

        User user = userRepository.findByUsername(userName)
                .orElseThrow(() -> new RunTimeException("The User with User Name: " + userName + " was not found"));
        List<String> roleStringList = new ArrayList<>();

            for (Role role: user.getRoles()){
                roleStringList.add(role.getName().toString());
            }
        if (roleStringList.contains("ROLE_ADMIN") || roleStringList.contains("ROLE_PRESIDENT") || roleStringList.contains("ROLE_SUPER_ADMIN")){
            inOut.setIscheck(true);
            inOut.setUser(user);
        }else {
            inOut.setIscheck(false);
            inOut.setUser(user);
        }
        inOut.setFile(dbUploadDocument);
        inOut.setAcrSession(acrSession);
        inOut.setGeneralAssembly(generalAssembly);
        inOut.setAcrCashCode(acrCashCode1);

            inOutDao.save(inOut);

        return new ResponseEntity<>(new ResponseMessage("the new In/Out has been successfully registered"), HttpStatus.CREATED);
    }

    @ApiOperation("Get All Valided InOutM")
    @GetMapping("listvalided")
    public List<InOutM> getinOutList() {
        try {

            List<InOutM> inOutList = inOutDao.findAll();
            List<InOutM> incomListFinal = new ArrayList<>();

            for (InOutM inOut : inOutList){
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date date1 = sdf.parse(inOut.getAcrSession().getAcrSessionEndDate());
                Date currentDate = new Date(System.currentTimeMillis());
                String currentDateString = sdf.format(currentDate);
                Date currentDateFormated = sdf.parse(currentDateString);
                if (date1.after(currentDateFormated) && inOut.getIscheck()){
                    incomListFinal.add(inOut);
                }
            }
            return incomListFinal;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @ApiOperation("Update InOutM")
    @PutMapping("update/{id}")
    public ResponseEntity<?> updateInOut(@Valid @RequestBody InOutM inOut, @PathVariable("id")int id){
        updateQueryInOut(id,inOut.getIntryArt(),inOut.getAmount(),inOut.getSender(),
                inOut.getPaymentReason(),inOut.getComment());
        return new ResponseEntity<>(new ResponseMessage("The InOutM with id: "+id+" was update successfully"), HttpStatus.OK);
    }

    private InOutM updateQueryInOut(
            int id,
            String originOfMoney,
            int amount,
            String benefitRecipients,
            String paymentReason,
            String comment) {
        InOutM inOut = inOutDao.findById(id)
                .orElseThrow(()-> new RunTimeException("The InOutM with Id: "+id +" was not found"));

        inOut.setIntryArt(originOfMoney);
        inOut.setAmount(amount);
        inOut.setSender(benefitRecipients);
        inOut.setPaymentReason(paymentReason);
        inOut.setComment(comment);

        inOutDao.save(inOut);
        return inOut;
    }

    @ApiOperation("Delete In/Out")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteIncom(@Valid @PathVariable("id") int id){
        InOutM inOut = inOutDao.findById(id)
                .orElseThrow(()-> new IllegalArgumentException("The In/Out with id: "+id+" was not found: "));
        inOutDao.delete(inOut);
        return new ResponseEntity<>(new ResponseMessage("The In/Out with id: "+id+" was deleted"), HttpStatus.OK);
    }


    @ApiOperation("Get All Invalided InOutM")
    @GetMapping("listinvalided")
    public List<InOutM> getInvalideInOutList() {
        try {

            List<InOutM> inOutList = inOutDao.findAll();
            List<InOutM> incomListFinal = new ArrayList<>();

            for (InOutM inOut : inOutList){
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date date1 = sdf.parse(inOut.getAcrSession().getAcrSessionEndDate());
                Date currentDate = new Date(System.currentTimeMillis());
                String currentDateString = sdf.format(currentDate);
                Date currentDateFormated = sdf.parse(currentDateString);
                if (date1.after(currentDateFormated) && !inOut.getIscheck()){
                    incomListFinal.add(inOut);
                }
            }
            return incomListFinal;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @ApiOperation("Valided a Incom")
    @GetMapping("valideinout/inoutId/{inoutId}/checkBoxValue/{checkBoxValue}")
    public ResponseEntity<?> validedkInOut(@PathVariable("inoutId") int inoutId, @PathVariable("checkBoxValue") boolean checkBoxValue){
        InOutM inOutM = inOutDao.findById(inoutId).orElseThrow(()-> new RunTimeException("The InOutM with Id: "+ inoutId +" was not found"));
        inOutM.setIscheck(checkBoxValue);
        inOutDao.save(inOutM);
        return new ResponseEntity<>(new ResponseMessage("The In/Out has been successfully Valided"), HttpStatus.CREATED);
    }
}
