package com.hz.acrKasseService.cashbox.controller;

import com.hz.acrKasseService.cashbox.dao.GeneralAssemblyDao;
import com.hz.acrKasseService.cashbox.dao.VereinKontoStandDao;
import com.hz.acrKasseService.cashbox.model.GeneralAssembly;
import com.hz.acrKasseService.cashbox.model.InOutM;
import com.hz.acrKasseService.cashbox.model.VereinKontoStandM;
import com.hz.acrKasseService.cashbox.model.payload.AgMemberBilan;
import com.hz.acrKasseService.cashbox.model.payload.BilanPayLoad;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@Api(description = "this service allows you to get Sommary Overview")
@RestController
@RequestMapping(value = "summaryoverview")
public class SummaryController {

    @Autowired
    private VereinKontoStandDao vereinKontoStandDao;

    @Autowired
    private GeneralAssemblyDao generalAssemblyDao;

    private Date currentDate = new Date(System.currentTimeMillis());
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    @ApiOperation(value = "Get all VereinKontoStand")
    @GetMapping("vkstands")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    public List<VereinKontoStandM> getAllVereinKontoStands() {
        List<VereinKontoStandM> vereinKontoStandMList = vereinKontoStandDao.findAll();
        return vereinKontoStandMList;
    }

    @ApiOperation("Get AG Bilan")
    @GetMapping("bilans")
    public ResponseEntity<?> getAgBilan() throws ParseException {

        List<GeneralAssembly> generalAssemblies = generalAssemblyDao.findAll();
        List<BilanPayLoad> bilanPayLoadList = new ArrayList<>();

        for (GeneralAssembly generalAssembly : generalAssemblies) {

            Date sessionEndDate = sdf.parse(generalAssembly.getAcrSession().getAcrSessionEndDate());
            Date agEndDate = sdf.parse(generalAssembly.getAgEndDate());

            String currentDateString = sdf.format(currentDate);
            Date currentDateFormated = sdf.parse(currentDateString);
            BilanPayLoad bilanPayLoad = new BilanPayLoad();


            if ((sessionEndDate.after(currentDateFormated)) && agEndDate.before(sessionEndDate)) {
                bilanPayLoad.setAgNr(generalAssembly.getGeneralAssemblyNummer());
                bilanPayLoad.setInput(getAgInputBilan(generalAssembly));
                bilanPayLoad.setOutput(getAgOutputBilan(generalAssembly));
                bilanPayLoad.setDelta(getAgInputBilan(generalAssembly) - getAgOutputBilan(generalAssembly));
                bilanPayLoad.setId(generalAssembly.getId());
                bilanPayLoad.setEndate(generalAssembly.getAgEndDate());
                bilanPayLoadList.add(bilanPayLoad);
            }
        }
        return new ResponseEntity<>(bilanPayLoadList, HttpStatus.OK);

    }


    private int getAgInputBilan(GeneralAssembly generalAssembly) {
        Set<InOutM> inOutMSet = generalAssembly.getInOuts();
        List<InOutM> inOutMList = new ArrayList<>();
        inOutMList.addAll(inOutMSet);
        int inputSum = 0;
        for (InOutM in : inOutMList) {
            if (in.getIscheck() && in.getIntryArt().equals("Input")) {
                inputSum += in.getAmount();
            }
        }
        return inputSum;
    }

    private int getAgOutputBilan(GeneralAssembly generalAssembly) {
        Set<InOutM> inOutMSet = generalAssembly.getInOuts();
        List<InOutM> inOutMList = new ArrayList<>();
        inOutMList.addAll(inOutMSet);
        int outputSum = 0;
        for (InOutM in : inOutMList) {
            if (in.getIscheck() && in.getIntryArt().equals("Output")) {
                outputSum += in.getAmount();
            }
        }
        return outputSum;
    }

    @ApiOperation("Get Session Member Bilan")
    @GetMapping("memberbilan")
    public ResponseEntity<?> getMemberBilan() throws ParseException {

        List<GeneralAssembly> generalAssemblies = generalAssemblyDao.findAll();
        List<AgMemberBilan> agMemberBilanList = new ArrayList<>();

        for (GeneralAssembly generalAssembly : generalAssemblies) {
            AgMemberBilan agMemberBilan = new AgMemberBilan();

            Date sessionEndDate = sdf.parse(generalAssembly.getAcrSession().getAcrSessionEndDate());
            Date agEndDate = sdf.parse(generalAssembly.getAgEndDate());

            String currentDateString = sdf.format(currentDate);
            Date currentDateFormated = sdf.parse(currentDateString);

            if ((sessionEndDate.after(currentDateFormated)) && agEndDate.before(sessionEndDate)) {
                agMemberBilan.setAgNr(generalAssembly.getGeneralAssemblyNummer());
                agMemberBilan.setId(generalAssembly.getId());
                agMemberBilan.setStudyMember(getStutyByAg(generalAssembly));
                agMemberBilan.setVeteranMember(getVeteranByAg(generalAssembly));
                agMemberBilan.setWorkingMember(getWorkingByAg(generalAssembly));
                agMemberBilan.setSum(getStutyByAg(generalAssembly) + getVeteranByAg(generalAssembly) + getWorkingByAg(generalAssembly));
                agMemberBilan.setEndate(generalAssembly.getAgEndDate());
                agMemberBilanList.add(agMemberBilan);
            }
        }
        return new ResponseEntity<>(agMemberBilanList, HttpStatus.OK);
    }

    private int getStutyByAg(GeneralAssembly generalAssembly) {
        List<InOutM> inOutMList = new ArrayList<>();
        inOutMList.addAll(generalAssembly.getInOuts());
        int studyMembersum = 0;
        for (InOutM in : inOutMList) {
            if (in.getAcrCashCode().getName().contains("Mitglied Etudiant")) {
                studyMembersum++;
            }
        }
        return studyMembersum;
    }

    private int getWorkingByAg(GeneralAssembly generalAssembly) {
        List<InOutM> inOutMList = new ArrayList<>();
        inOutMList.addAll(generalAssembly.getInOuts());
        int workingMembersum = 0;
        for (InOutM in : inOutMList) {
            if (in.getAcrCashCode().getName().contains("Mitglied Travailleur")) {
                workingMembersum++;
            }
        }
        return workingMembersum;
    }


    private int getVeteranByAg(GeneralAssembly generalAssembly) {
        List<InOutM> inOutMList = new ArrayList<>();
        inOutMList.addAll(generalAssembly.getInOuts());
        int veteranMembersum = 0;
        for (InOutM in : inOutMList) {
            if (in.getAcrCashCode().getName().contains("Mitglied Veteran")) {
                veteranMembersum++;
            }
        }
        return veteranMembersum;
    }
}
