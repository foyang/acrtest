package com.hz.acrKasseService.cashbox.controller;

import com.hz.acrKasseService.cashbox.dao.ContactPersonDao;
import com.hz.acrKasseService.cashbox.dao.InOutDao;
import com.hz.acrKasseService.cashbox.dao.SympathisantDao;
import com.hz.acrKasseService.cashbox.dao.VereinKontoStandDao;
import com.hz.acrKasseService.cashbox.model.*;
import com.hz.acrKasseService.cashbox.model.payload.Activity;
import com.hz.acrKasseService.cashbox.model.payload.BilanPayLoad;
import com.hz.acrKasseService.cashbox.model.payload.Member;
import com.hz.acrKasseService.cashbox.model.payload.SympathisantPayload;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@Api(description = "this service allows you to get InOutM overview")
@RestController
@RequestMapping(value = "overview")
public class OverviewController {

    @Autowired
    private InOutDao inOutDao;

    @Autowired
    private SympathisantDao sympathisantDao;

    @Autowired
    private ContactPersonDao contactPersonDao;

    @Autowired
    private VereinKontoStandDao vereinKontoStandDao;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("inOutlist")
    public ResponseEntity<?> getAllIncomOverview() throws ParseException {

        List<Overview> incomOverviewList = new ArrayList<>();
        List<InOutM> allIncom = inOutDao.findAll();

        for (InOutM inOut : allIncom){
            if (notExpiratedSession(inOut.getAcrSession().getAcrSessionEndDate()) && inOut.getIscheck()){
                Overview inOutOverview = new Overview();

                inOutOverview.setAgId(inOut.getId());
                inOutOverview.setAmount(inOut.getAmount());
                inOutOverview.setSender(inOut.getSender());
                inOutOverview.setComment(inOut.getComment());
                inOutOverview.setPaymentReason(inOut.getPaymentReason());
                if (inOut.getFile() != null){
                    inOutOverview.setBillId(inOut.getFile().getId());
                }
                inOutOverview.setCodeName(inOut.getAcrCashCode().getName());
                inOutOverview.setSessionId(inOut.getAcrSession().getAcrSessionNumber());
                inOutOverview.setAgId(inOut.getGeneralAssembly().getGeneralAssemblyNummer());
                inOutOverview.setIntryDate(inOut.getIntryDate());
                if (inOut.getFile() != null){
                    inOutOverview.setBillType(inOut.getFile().getFileType());
                    inOutOverview.setBillName(inOut.getFile().getFileName());
                }
                inOutOverview.setIntryArt(inOut.getIntryArt());
                inOutOverview.setManager(inOut.getManager());

                incomOverviewList.add(inOutOverview);
            }

        }
        return new ResponseEntity<>(incomOverviewList , HttpStatus.OK);
    }


    private Boolean notExpiratedSession(String dateToCheck) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date1 = sdf.parse(dateToCheck);
        Date currentDate = new Date(System.currentTimeMillis());
        String currentDateString = sdf.format(currentDate);
        Date currentDateFormated = sdf.parse(currentDateString);
        if (date1.after(currentDateFormated)){
            return true;
        }
        return false;
    }

    @GetMapping("getInputSum")
    public ResponseEntity<?> getsum() throws ParseException {
        int sum = 0;
        List<InOutM> allInOut = inOutDao.findAll();
        for (InOutM inOut : allInOut){
            if (notExpiratedSession(inOut.getAcrSession().getAcrSessionEndDate())){
                if (inOut.getIntryArt().equals("Input") && inOut.getIscheck())
                sum += inOut.getAmount();
            }
        }
        return new ResponseEntity<>(sum , HttpStatus.OK);
    }

    @GetMapping("getOutputSum")
    public ResponseEntity<?> getOutputsum() throws ParseException {
        int sum = 0;
        List<InOutM> allInOut = inOutDao.findAll();
        for (InOutM inOut : allInOut){
            if (notExpiratedSession(inOut.getAcrSession().getAcrSessionEndDate())){
                if (inOut.getIntryArt().equals("Output") && inOut.getIscheck())
                    sum += inOut.getAmount();
            }
        }
        return new ResponseEntity<>(sum , HttpStatus.OK);
    }


    @GetMapping("getMemberList")
    public ResponseEntity<?> getMemberList() throws ParseException {
        List<InOutM> allInOut = inOutDao.findAll();
        List<Member> memberList = new ArrayList<>();

        for (InOutM inOut : allInOut){
            Member member = new Member();
            if (notExpiratedSession(inOut.getAcrSession().getAcrSessionEndDate())){
                if (inOut.getIntryArt().equals("Input") && inOut.getAcrCashCode().getName().contains("Mitglied") && inOut.getIscheck()){
                    member.setSessionNr(inOut.getAcrSession().getAcrSessionNumber());
                    member.setAmount(inOut.getAmount());
                    member.setIntryDate(inOut.getIntryDate());
                    member.setMemberTyp(inOut.getAcrCashCode().getName());
                    member.setName(inOut.getSender());
                    memberList.add(member);

                }
            }
        }
        return new ResponseEntity<>(memberList , HttpStatus.OK);
    }



    @GetMapping("getUncheckMemberList")
    public ResponseEntity<?> getUncheckMemberList() throws ParseException {
        List<InOutM> allInOut = inOutDao.findAll();
        List<Member> memberList = new ArrayList<>();

        for (InOutM inOut : allInOut){
            Member member = new Member();
            if (notExpiratedSession(inOut.getAcrSession().getAcrSessionEndDate())){
                if (inOut.getIntryArt().equals("Input") && inOut.getAcrCashCode().getName().contains("Mitglied") && !inOut.getIscheck()){
                    member.setSessionNr(inOut.getAcrSession().getAcrSessionNumber());
                    member.setAmount(inOut.getAmount());
                    member.setIntryDate(inOut.getIntryDate());
                    member.setMemberTyp(inOut.getAcrCashCode().getName());
                    member.setName(inOut.getSender());
                    memberList.add(member);

                }
            }
        }
        return new ResponseEntity<>(memberList , HttpStatus.OK);
    }


    @GetMapping("getSportMemberList")
    public ResponseEntity<?> getSportMemberList() throws ParseException {
        List<InOutM> allInOut = inOutDao.findAll();
        List<Member> memberList = new ArrayList<>();

        for (InOutM inOut : allInOut){
            Member member = new Member();
            if (notExpiratedSession(inOut.getAcrSession().getAcrSessionEndDate())){
                if (inOut.getIntryArt().equals("Input") && inOut.getAcrCashCode().getName().contains("Sportif") && inOut.getIscheck()){
                    member.setSessionNr(inOut.getAcrSession().getAcrSessionNumber());
                    member.setIntryDate(inOut.getIntryDate());
                    member.setMemberTyp(inOut.getAcrCashCode().getName());
                    member.setName(inOut.getSender());
                    member.setCode(inOut.getAcrCashCode().getName());
                    memberList.add(member);

                }
            }
        }
        return new ResponseEntity<>(memberList , HttpStatus.OK);
    }


    @GetMapping("getMemberCount")
    public ResponseEntity<?> getMemberCount() throws ParseException {

        List<InOutM> allInOut = inOutDao.findAll();
        List<Member> memberList = new ArrayList<>();

        for (InOutM inOut : allInOut){
            Member member = new Member();
            if (notExpiratedSession(inOut.getAcrSession().getAcrSessionEndDate())){
                if (inOut.getIntryArt().equals("Input") && inOut.getAcrCashCode().getName().contains("Mitglied") && inOut.getIscheck()){
                    member.setSessionNr(inOut.getAcrSession().getAcrSessionNumber());
                    member.setAmount(inOut.getAmount());
                    member.setIntryDate(inOut.getIntryDate());
                    member.setMemberTyp(inOut.getAcrCashCode().getName());
                    member.setName(inOut.getSender());

                    memberList.add(member);
                }
            }
        }
        return new ResponseEntity<>(memberList.size() , HttpStatus.OK);
    }

    @GetMapping("sympathisantMembers")
    public ResponseEntity<?> getSympathisantMember() throws ParseException {

        List<Sympathisant> sympathisantList = sympathisantDao.findAll();

        List<SympathisantPayload> sympathisants = new ArrayList<>();

        for (Sympathisant sympathisant : sympathisantList){
            SympathisantPayload sympathisantPayload = new SympathisantPayload();

            if (notExpiratedSession(sympathisant.getAcrSession().getAcrSessionEndDate())){
                    sympathisantPayload.setSessionNr(sympathisant.getAcrSession().getAcrSessionNumber());
                    sympathisantPayload.setIntryDate(sympathisant.getIntryDate());
                    sympathisantPayload.setName(sympathisant.getName());
                    sympathisants.add(sympathisantPayload);
            }
        }
        return new ResponseEntity<>(sympathisants , HttpStatus.OK);
    }

    /* ToDo problem when we start with the output */
    @ApiOperation("Get Activity")
    @GetMapping("activitys")
    public ResponseEntity<?> getCtivityList(){
        List<Activity> activityList = new ArrayList<>();
        List<Activity> activityFinalList = new ArrayList<>();
        List<InOutM> inOutMList = inOutDao.findAll();
        for (InOutM inOutM: inOutMList){
            if (inOutM.getAcrCashCode().getName().equals("Aktivität Club Culturel") || inOutM.getAcrCashCode().getName().equals("Aktivität Vorstand")
                    ||inOutM.getAcrCashCode().getName().equals("Aktivität Club Sportif")){
                Activity activity = new Activity();

                int outputAmount = 0;
                int inputAmount = 0;

                if (inOutM.getIntryArt().equals("Input")){
                    inputAmount = inOutM.getAmount();
                }
                if (inOutM.getIntryArt().equals("Output")){
                    outputAmount = inOutM.getAmount();
                }
                activity.setActivityName(inOutM.getPaymentReason());
                activity.setAgNr(inOutM.getGeneralAssembly().getGeneralAssemblyNummer());
                activity.setDescription(inOutM.getComment());
                activity.setInput(inputAmount);
                activity.setOutput(outputAmount);
                activity.setIntryDate(inOutM.getIntryDate());
                activity.setSessionNr(inOutM.getAcrSession().getAcrSessionNumber());
                activity.setCode(inOutM.getAcrCashCode().getName());
                activityList.add(activity);
            }
        }


        for (int i = 0; i < activityList.size(); i++) {
            Activity activity1 = new Activity();
            Activity activity = new Activity();

            int montantIIn = 0;
            int montantIOut = 0;
            if (activityList.get(i).getInput() != 0){
                 montantIIn = activityList.get(i).getInput();
            }else {
                montantIOut = activityList.get(i).getOutput();
            }
            for (int j = i+1; j <activityList.size() ; j++) {
                int montantJ = 0;
                if(activityList.get(i).getActivityName().equals(activityList.get(j).getActivityName())
                && activityList.get(i).getCode().equals(activityList.get(j).getCode())){
                    if (activityList.get(j).getInput() != 0){
                        montantJ = activityList.get(j).getInput();
                    }else {
                        montantJ = activityList.get(j).getOutput();
                    }
//                    logger.info("MontantI: " + montantI+ " MontantJ: "+montantJ+" :  Diff: "+(montantI - montantJ)+
//                            " Code i: "+activityList.get(i).getCode() +"Code j: "+activityList.get(j).getCode());
                    activity.setCode(activityList.get(j).getCode());
                    activity.setSessionNr(activityList.get(j).getSessionNr());
                    activity.setIntryDate(activityList.get(j).getIntryDate());
                    activity.setOutput(montantJ);
                    activity.setInput(montantIIn);
                    activity.setDescription(activityList.get(j).getDescription());
                    activity.setAgNr(activityList.get(j).getAgNr());
                    activity.setActivityName(activityList.get(j).getActivityName());
                    activity.setInOutDiff(montantIIn-montantJ);
                    activityFinalList.add(activity);

                    activityList.remove(j);

                }else {

                }
            }

            activity1.setCode(activityList.get(i).getCode());
            activity1.setSessionNr(activityList.get(i).getSessionNr());
            activity1.setIntryDate(activityList.get(i).getIntryDate());
            activity1.setOutput(montantIOut);
            activity1.setInput(montantIIn);
            activity1.setDescription(activityList.get(i).getDescription());
            activity1.setAgNr(activityList.get(i).getAgNr());
            activity1.setActivityName(activityList.get(i).getActivityName());
            activity1.setInOutDiff(montantIIn-montantIOut);

            if (!activityFinalList.contains(activity)){
                activityFinalList.add(activity1);
            }
        }
        return new ResponseEntity<>(activityFinalList , HttpStatus.OK);
    }

    @ApiOperation("Get All Contact Person")
    @GetMapping("contacts")
    public ResponseEntity<?> getAllContactPerson(){
        List<ContactPerson> contactPersonList = contactPersonDao.findAll();
        return new ResponseEntity<>(contactPersonList , HttpStatus.OK);

    }
}
