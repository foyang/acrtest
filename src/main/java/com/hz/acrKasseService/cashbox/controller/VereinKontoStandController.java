package com.hz.acrKasseService.cashbox.controller;

import com.hz.acrKasseService.cashbox.dao.VereinKontoStandDao;
import com.hz.acrKasseService.cashbox.model.VereinKontoStandM;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(description = "this Service allows you to add new VereinKontoStand.")
@RequestMapping(value = "vereinKontoStand")
@PreAuthorize("hasRole('SUPER_ADMIN') or hasRole('PRESIDENT') or hasRole('ADMIN') or hasRole('HELP')")
public class VereinKontoStandController {

    @Autowired
    private VereinKontoStandDao vereinKontoStandDao;

    @ApiOperation(value = "Add new VereinKontoStand")
    @PostMapping(value = "add")
    public ResponseEntity<?> addVereinKontoStand(@Valid @RequestBody VereinKontoStandM vereinKontoStandM) {
        vereinKontoStandDao.save(vereinKontoStandM);
        return new ResponseEntity<>(new ResponseMessage("The new VereinKontoStand has been successfully registered"), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Delete VereinKontoStand")
    @DeleteMapping(value = "delete/{id}")
    public ResponseEntity<?> deleteVereinKontoStand(@Valid @PathVariable("id") int id){
        VereinKontoStandM vereinKontoStandM = vereinKontoStandDao.findById(id)
                .orElseThrow(()-> new IllegalArgumentException("The VereinKontoStand with id: "+id+" was not found: "));
        vereinKontoStandDao.delete(vereinKontoStandM);
        return new ResponseEntity<>(new ResponseMessage("The VereinKontoStand with id: "+id+" was deleted"), HttpStatus.OK);
    }

    @ApiOperation(value = "Update VereinKontoStand")
    @PutMapping("update/{id}")
    public ResponseEntity<?> UpdateVereinKontoStand(@Valid @RequestBody VereinKontoStandM vereinKontoStandM, @PathVariable("id") int id){
        updateVereinKontoStand(id, vereinKontoStandM.getDate(), vereinKontoStandM.getAmount());
        return new ResponseEntity<>(new ResponseMessage("The VereinKontoStand with id: "+id+" was update successfully"), HttpStatus.OK);
    }

    public VereinKontoStandM updateVereinKontoStand(int id, String date, int amount){
        VereinKontoStandM vereinKontoStandM = vereinKontoStandDao.findById(id)
                .orElseThrow(() -> new RuntimeException("The Sympathisan with id: "+id+" was not found"));
        vereinKontoStandM.setAmount(amount);
        vereinKontoStandM.setDate(date);
        vereinKontoStandDao.save(vereinKontoStandM);
        return vereinKontoStandM;
    }

    @ApiOperation(value = "Get all VereinKontoStand")
    @GetMapping("list")
    public List<VereinKontoStandM> getAllVereinKontoStands(){
        List<VereinKontoStandM> vereinKontoStandMList = vereinKontoStandDao.findAll();
        return vereinKontoStandMList;
    }
}
