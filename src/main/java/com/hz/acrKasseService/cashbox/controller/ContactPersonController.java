package com.hz.acrKasseService.cashbox.controller;

import com.hz.acrKasseService.cashbox.dao.ContactPersonDao;
import com.hz.acrKasseService.cashbox.model.ContactPerson;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(description = "this Service allows you to add new Contact Person.")
@RequestMapping(value = "contactperson")
@PreAuthorize("hasRole('SUPER_ADMIN') or hasRole('PRESIDENT') or hasRole('ADMIN')")
public class ContactPersonController {

    @Autowired
    private ContactPersonDao contactPersonDao;

    @ApiOperation("Add new Contact Person")
    @PostMapping("add")
    public ResponseEntity<?> addContactPerson(@Valid @RequestBody ContactPerson contactPerson){
        ContactPerson contactPerson1 = contactPersonDao.findByName(contactPerson.getName());
        if (contactPerson.getName() == null){
            return new ResponseEntity<>(new ResponseMessage("Contact Person name can not be null."),
                    HttpStatus.NOT_ACCEPTABLE);
        }
        if (contactPerson1 == null){
            contactPersonDao.save(contactPerson);
        }
        return new ResponseEntity<>(new ResponseMessage("The new Contact Person has been successfully registered"), HttpStatus.CREATED);
    }

    @ApiOperation("Delete a Contact Person")
    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteContactPerson(@Valid @PathVariable("id") int id ){
        ContactPerson contactPerson = contactPersonDao.findById(id).orElseThrow(()-> new IllegalArgumentException("The Contact Person with id: "+id+" was not found: "));

        if (contactPerson == null){
            return new ResponseEntity<>(new ResponseMessage("The Contact Person with id: "+id+" was not found: "), HttpStatus.BAD_REQUEST);
        }
        contactPersonDao.delete(contactPerson);
        return new ResponseEntity<>(new ResponseMessage("The Contact Person with id: "+id+" was deleted"), HttpStatus.OK);
    }

    @ApiOperation(value = "Update Contact Person")
    @PutMapping("update/{id}")
    public ResponseEntity<?> UpdateContactPerson(@Valid @RequestBody ContactPerson contactPerson, @PathVariable("id") int id){
        update(id, contactPerson.getName(), contactPerson.getEmail(), contactPerson.getAdress(), contactPerson.getFonction(), contactPerson.getOrt(),
                contactPerson.getPhoneNumber(), contactPerson.getPlz());
        return new ResponseEntity<>(new ResponseMessage("The Code with id: "+id+" was update successfully"), HttpStatus.OK);
    }

    public ContactPerson update(int id, String name, String email, String adress, String fonction, String ort, String phoneNr, int plz){
        ContactPerson contactPerson = contactPersonDao.findById(id)
                .orElseThrow(() -> new RuntimeException("The Contact Person with id: "+id+" was not found"));
        contactPerson.setName(name);
        contactPerson.setAdress(adress);
        contactPerson.setEmail(email);
        contactPerson.setFonction(fonction);
        contactPerson.setOrt(ort);
        contactPerson.setPhoneNumber(phoneNr);
        contactPerson.setPlz(plz);
        contactPersonDao.save(contactPerson);
        return contactPerson;
    }

    @ApiOperation(value = "Get all Contact Person")
    @GetMapping("list")
    public List<ContactPerson> getAllCodes(){
        List<ContactPerson> listCode = contactPersonDao.findAll();
        return listCode;
    }
}
