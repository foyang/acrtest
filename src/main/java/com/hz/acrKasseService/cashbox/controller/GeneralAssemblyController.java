package com.hz.acrKasseService.cashbox.controller;

import com.hz.acrKasseService.cashbox.dao.AcrSessionDao;
import com.hz.acrKasseService.cashbox.dao.GeneralAssemblyDao;
import com.hz.acrKasseService.cashbox.model.AcrSession;
import com.hz.acrKasseService.cashbox.model.GeneralAssembly;
import com.hz.acrKasseService.exception.RunTimeException;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(description = "this Service allows you to add a new General Assembly")
@RequestMapping(value = "inout/generalassembly")
public class GeneralAssemblyController {

    @Autowired
    private GeneralAssemblyDao generalAssemblyDao;

    @Autowired
   private AcrSessionDao acrSessionDao;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private Date currentDate = new Date(System.currentTimeMillis());
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    @ApiOperation(value = "add new General Assembly")
    @PostMapping(value = "session/{session_id}/add")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    public ResponseEntity<?> addGeneralAssembly(@Valid @RequestBody GeneralAssembly generalAssembly,@PathVariable("session_id") int session_id) throws ParseException {
        GeneralAssembly generalAssemblyId =  generalAssemblyDao.findBygeneralAssemblyNummer(generalAssembly.getGeneralAssemblyNummer());

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date1 = sdf.parse(generalAssembly.getAgStartDate());
        Date date2 = sdf.parse(generalAssembly.getAgEndDate());
        Date currentDate = new Date(System.currentTimeMillis());

        String currentDateString = sdf.format(currentDate);
        Date currentDateFormated = sdf.parse(currentDateString);

        AcrSession acrSession = acrSessionDao.findById(session_id)
                .orElseThrow(()-> new RunTimeException("The ACR-Session with id: "+session_id+" was not found"));

        if (generalAssemblyId == null){
            if (!date1.after(date2) && !date1.before(currentDateFormated) && !date2.before(currentDateFormated)){
                generalAssembly.setAcrSession(acrSession);
                generalAssemblyDao.save(generalAssembly);
            }else {
                throw new RunTimeException("The date of the General Assembly does not match or is in the past");
            }
        }else {
            throw new RunTimeException("The General Assembly with number "+generalAssembly.getGeneralAssemblyNummer()+" already exist");
        }
        return new ResponseEntity<>(new ResponseMessage("the new General Assembly has been successfully registered"), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update General Assembly")
    @PutMapping(value = "update/{id}")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    public ResponseEntity<?> updateGeneralAssembly(@Valid @RequestBody GeneralAssembly generalAssembly,  @PathVariable("id") int id){
        updateGeneralAssembly(id,generalAssembly.getGeneralAssemblyNummer(),generalAssembly.getAcrSession(), generalAssembly.getAgStartDate(), generalAssembly.getAgEndDate());
        return new ResponseEntity<>(new ResponseMessage("The General Assembly with Number: "+generalAssembly.getGeneralAssemblyNummer()+" and id: "+id+" was update successfully"), HttpStatus.OK);
    }

    private GeneralAssembly updateGeneralAssembly(int id, int generalAssemblyNummer, AcrSession acrSession, String agStartDate, String agEndDate){
        GeneralAssembly generalAssembly = generalAssemblyDao.findById(id)
                .orElseThrow(() -> new RuntimeException("The General Assembly with Number: "+generalAssemblyNummer +" and id: "+id+" was not found"));
        generalAssembly.setGeneralAssemblyNummer(generalAssemblyNummer);
        if (acrSession != null){
            generalAssembly.setAcrSession(acrSession);
        }

        generalAssembly.setAgStartDate(agStartDate);
        generalAssembly.setAgEndDate(agEndDate);
        generalAssemblyDao.save(generalAssembly);
        return generalAssembly;
    }

    @ApiOperation(value = "Delete General Assembly")
    @DeleteMapping(value = "delete/{id}")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteGeneralAssembly(@Valid @PathVariable("id") int id){
        GeneralAssembly generalAssembly = generalAssemblyDao.findById(id)
                .orElseThrow(()-> new IllegalArgumentException("The ACR General Assembly with id: "+id+" was not found: "));
        generalAssemblyDao.delete(generalAssembly);
        return new ResponseEntity<>(new ResponseMessage("The ACR General Assembly with id: "+id+" was deleted"), HttpStatus.OK);
    }

    @ApiOperation(value = "Get All General Assembly")
    @GetMapping(value = "list")
    public ResponseEntity<?> getListGeneralAssembly(){
        try {
            return new ResponseEntity<>(generalAssemblyDao.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "Get Current General Assembly")
    @GetMapping(value = "currentags")
    public ResponseEntity<?> getCurrentGeneralAssembly() throws ParseException {

        List<GeneralAssembly> generalAssemblyList = new ArrayList<>();
        List<GeneralAssembly> generalAssemblies = generalAssemblyDao.findAll();

        for (GeneralAssembly generalAssembly:generalAssemblies){
            Date sessionEndDate = sdf.parse(generalAssembly.getAcrSession().getAcrSessionEndDate());
            Date agEndDate = sdf.parse(generalAssembly.getAgEndDate());

            String currentDateString = sdf.format(currentDate);
            Date currentDateFormated = sdf.parse(currentDateString);

            if ((sessionEndDate.after(currentDateFormated)) && agEndDate.before(sessionEndDate)){
                    generalAssemblyList.add(generalAssembly);
            }
        }
        return new ResponseEntity<>(generalAssemblyList, HttpStatus.OK);
    }
}
