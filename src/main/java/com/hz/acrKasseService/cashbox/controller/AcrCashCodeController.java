package com.hz.acrKasseService.cashbox.controller;

import com.hz.acrKasseService.cashbox.dao.AcrCahsCodeDao;
import com.hz.acrKasseService.cashbox.model.AcrCashCode;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(description = "this Service allows you to add new Code.")
@RequestMapping(value = "inout/code")
public class AcrCashCodeController {

    @Autowired
    private AcrCahsCodeDao acrCahsCodeDao;

    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    @ApiOperation(value = "add new Code")
    @PostMapping(value = "add")
    public ResponseEntity<?> addCode(@Valid @RequestBody AcrCashCode acrCashCode) {
        AcrCashCode cashCodeName = acrCahsCodeDao.findByName(acrCashCode.getName());
        if (acrCashCode.getName() == null){
            return new ResponseEntity<>(new ResponseMessage("Code name can not be null."),
                    HttpStatus.NOT_ACCEPTABLE);
        }
        if (cashCodeName == null){
            acrCahsCodeDao.save(acrCashCode);
        }else {
            //throw new RunTimeException("code with name "+acrCashCode.getName()+" already exist");
            return new ResponseEntity<>(new ResponseMessage("Code with name "+acrCashCode.getName()+" already exist"),
                    HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity<>(new ResponseMessage("The new code has been successfully registered"), HttpStatus.CREATED);
    }

    @ApiOperation(value = "delete Code")
    @DeleteMapping(value = "delete/{id}")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteCode(@Valid @PathVariable("id") int id){
        AcrCashCode acrCashCode = acrCahsCodeDao.findById(id)
                .orElseThrow(()-> new IllegalArgumentException("The Code with id: "+id+" was not found: "));
        acrCahsCodeDao.delete(acrCashCode);
        return new ResponseEntity<>(new ResponseMessage("The Code with id: "+id+" was deleted"), HttpStatus.OK);
    }

    @ApiOperation(value = "Update Code")
    @PutMapping("update/{id}")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    public ResponseEntity<?> UpdateCode(@Valid @RequestBody AcrCashCode acrCashCode, @PathVariable("id") int id){
        update(id, acrCashCode.getName(), acrCashCode.getDescription());
        return new ResponseEntity<>(new ResponseMessage("The Code with id: "+id+" was update successfully"), HttpStatus.OK);
    }

    public AcrCashCode update(int id, String name, String description){
        AcrCashCode acrCashCode = acrCahsCodeDao.findById(id)
                .orElseThrow(() -> new RuntimeException("The Code with id: "+id+" was not found"));
        acrCashCode.setName(name);
        acrCashCode.setDescription(description);
        acrCahsCodeDao.save(acrCashCode);
        return acrCashCode;
    }

    @ApiOperation(value = "Get all Codes")
    @GetMapping("list")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN') or hasRole('HELP')")
    public List<AcrCashCode> getAllCodes(){
        List<AcrCashCode> listCode = acrCahsCodeDao.findAll();
        return listCode;
    }
}
