package com.hz.acrKasseService.cashbox.model;

import javax.persistence.Column;

public class Overview {

   private int id ;
    private int sessionId;
    private int agId;
    private int amount;
    private String CodeName;
    private String sender;
    private String paymentReason;

    @Column( length = 100000 )
    private String comment;

    private String billId;
    private String intryDate;
    private String billType;
    private String billName;
    private String manager;
    private int amountSumme;
    private String intryArt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public int getAgId() {
        return agId;
    }

    public void setAgId(int agId) {
        this.agId = agId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCodeName() {
        return CodeName;
    }

    public void setCodeName(String codeName) {
        CodeName = codeName;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getPaymentReason() {
        return paymentReason;
    }

    public void setPaymentReason(String paymentReason) {
        this.paymentReason = paymentReason;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getIntryDate() {
        return intryDate;
    }

    public void setIntryDate(String intryDate) {
        this.intryDate = intryDate;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getBillName() {
        return billName;
    }

    public void setBillName(String billName) {
        this.billName = billName;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public int getAmountSumme() {
        return amountSumme;
    }

    public void setAmountSumme(int amountSumme) {
        this.amountSumme = amountSumme;
    }

    public String getIntryArt() {
        return intryArt;
    }

    public void setIntryArt(String intryArt) {
        this.intryArt = intryArt;
    }
}
