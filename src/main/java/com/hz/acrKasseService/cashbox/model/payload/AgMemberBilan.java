package com.hz.acrKasseService.cashbox.model.payload;

public class AgMemberBilan {

    private int id;
    private int agNr;
    private int studyMember;
    private int workingMember;
    private int veteranMember;
    private int sum;
    private String endate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAgNr() {
        return agNr;
    }

    public void setAgNr(int agNr) {
        this.agNr = agNr;
    }

    public int getStudyMember() {
        return studyMember;
    }

    public void setStudyMember(int studyMember) {
        this.studyMember = studyMember;
    }

    public int getWorkingMember() {
        return workingMember;
    }

    public void setWorkingMember(int workingMember) {
        this.workingMember = workingMember;
    }

    public int getVeteranMember() {
        return veteranMember;
    }

    public void setVeteranMember(int veteranMember) {
        this.veteranMember = veteranMember;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public String getEndate() {
        return endate;
    }

    public void setEndate(String endate) {
        this.endate = endate;
    }
}
