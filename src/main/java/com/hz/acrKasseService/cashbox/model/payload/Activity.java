package com.hz.acrKasseService.cashbox.model.payload;

public class Activity {

    private int sessionNr;
    private int agNr;
    private String intryDate;
    private String activityName;
    private int input;
    private int output;
    private String description;
    private String code;
    private int inOutDiff;

    public int getSessionNr() {
        return sessionNr;
    }

    public void setSessionNr(int sessionNr) {
        this.sessionNr = sessionNr;
    }

    public int getAgNr() {
        return agNr;
    }

    public void setAgNr(int agNr) {
        this.agNr = agNr;
    }

    public String getIntryDate() {
        return intryDate;
    }

    public void setIntryDate(String intryDate) {
        this.intryDate = intryDate;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public int getInput() {
        return input;
    }

    public void setInput(int input) {
        this.input = input;
    }

    public int getOutput() {
        return output;
    }

    public void setOutput(int output) {
        this.output = output;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getInOutDiff() {
        return inOutDiff;
    }

    public void setInOutDiff(int inOutDiff) {
        this.inOutDiff = inOutDiff;
    }
}
