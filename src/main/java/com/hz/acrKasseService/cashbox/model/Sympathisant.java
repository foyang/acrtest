package com.hz.acrKasseService.cashbox.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "sympathisant")
public class Sympathisant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String name;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String intryDate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "acrSession_id",nullable = false)
    @JsonIgnore
    private AcrSession acrSession;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntryDate() {
        return intryDate;
    }

    public void setIntryDate(String intryDate) {
        this.intryDate = intryDate;
    }

    public AcrSession getAcrSession() {
        return acrSession;
    }

    public void setAcrSession(AcrSession acrSession) {
        this.acrSession = acrSession;
    }
}
