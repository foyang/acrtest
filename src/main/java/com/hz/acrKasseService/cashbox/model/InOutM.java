package com.hz.acrKasseService.cashbox.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hz.acrKasseService.security.model.User;

import javax.persistence.*;

@Entity
@Table(name = "inoutm")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class InOutM {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String intryDate;

    @Column(nullable = false)
    private String intryArt;

    @Column(nullable = false)
    private int amount;

    @Column(nullable = false)
    private String sender;


    @Column(nullable = false)
    private String paymentReason;

    @Column( length = 100000 )
    private String comment;

    @Column(nullable = false)
    private String manager;

    private Boolean ischeck;

    @OneToOne
    @JoinColumn(name = "AcrCashCode_id", referencedColumnName = "id",nullable = false)
    private AcrCashCode acrCashCode;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id",nullable = true)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "acrSession_id",nullable = false)
    @JsonIgnore
    //@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    //@JsonBackReference
    private AcrSession acrSession;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "generalAssembly_id",nullable = false)
    @JsonIgnore
    private GeneralAssembly generalAssembly;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "file_id", referencedColumnName = "id",nullable = true)
    private DbUploadDocument file;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIntryDate() {
        return intryDate;
    }

    public void setIntryDate(String intryDate) {
        this.intryDate = intryDate;
    }

    public String getIntryArt() {
        return intryArt;
    }

    public void setIntryArt(String intryArt) {
        this.intryArt = intryArt;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getPaymentReason() {
        return paymentReason;
    }

    public void setPaymentReason(String paymentReason) {
        this.paymentReason = paymentReason;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public AcrCashCode getAcrCashCode() {
        return acrCashCode;
    }

    public void setAcrCashCode(AcrCashCode acrCashCode) {
        this.acrCashCode = acrCashCode;
    }

    public AcrSession getAcrSession() {
        return acrSession;
    }

    public void setAcrSession(AcrSession acrSession) {
        this.acrSession = acrSession;
    }

    public GeneralAssembly getGeneralAssembly() {
        return generalAssembly;
    }

    public void setGeneralAssembly(GeneralAssembly generalAssembly) {
        this.generalAssembly = generalAssembly;
    }

    public DbUploadDocument getFile() {
        return file;
    }

    public void setFile(DbUploadDocument file) {
        this.file = file;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public Boolean getIscheck() {
        return ischeck;
    }

    public void setIscheck(Boolean ischeck) {
        this.ischeck = ischeck;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
