package com.hz.acrKasseService.cashbox.model.payload;

public class SympathisantPayload {


    private int id;
    private String name;
    private String intryDate;
    private int sessionNr;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntryDate() {
        return intryDate;
    }

    public void setIntryDate(String intryDate) {
        this.intryDate = intryDate;
    }

    public int getSessionNr() {
        return sessionNr;
    }

    public void setSessionNr(int sessionNr) {
        this.sessionNr = sessionNr;
    }
}
