package com.hz.acrKasseService.cashbox.model.payload;

public class BilanPayLoad {

    private int id;
    private int agNr;
    private int input;
    private int output;
    private int delta;
    private String endate;

    public int getAgNr() {
        return agNr;
    }

    public void setAgNr(int agNr) {
        this.agNr = agNr;
    }

    public int getInput() {
        return input;
    }

    public void setInput(int input) {
        this.input = input;
    }

    public int getOutput() {
        return output;
    }

    public void setOutput(int output) {
        this.output = output;
    }

    public int getDelta() {
        return delta;
    }

    public void setDelta(int delta) {
        this.delta = delta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEndate() {
        return endate;
    }

    public void setEndate(String endate) {
        this.endate = endate;
    }
}
