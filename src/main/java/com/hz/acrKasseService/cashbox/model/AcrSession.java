package com.hz.acrKasseService.cashbox.model;

import com.hz.acrKasseService.members.MemberRegistration;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Table(name = "AcrSession")
public class AcrSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true, nullable = false)
    private int acrSessionNumber;

    @NotBlank
    private String arcSessionStartDate;

    @NotBlank
    private String acrSessionEndDate;

     @ManyToMany(mappedBy = "acrSession", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
     private Set<InOutM> inOuts;

     @ManyToMany(mappedBy = "acrSession", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
     private Set<Sympathisant> sympathisants;


    @ManyToMany(mappedBy = "acrSession", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<GeneralAssembly> generalAssemblies;

    @ManyToMany(mappedBy = "acrSession", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<MemberRegistration> memberRegistrations;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAcrSessionNumber() {
        return acrSessionNumber;
    }

    public void setAcrSessionNumber(int acrSessionNumber) {
        this.acrSessionNumber = acrSessionNumber;
    }

    public String getArcSessionStartDate() {
        return arcSessionStartDate;
    }

    public void setArcSessionStartDate(String arcSessionStartDate) {
        this.arcSessionStartDate = arcSessionStartDate;
    }

    public String getAcrSessionEndDate() {
        return acrSessionEndDate;
    }

    public void setAcrSessionEndDate(String acrSessionEndDate) {
        this.acrSessionEndDate = acrSessionEndDate;
    }

     public Set<InOutM> getInOuts() {
        return inOuts;
    }

    public void setInOuts(Set<InOutM> inOuts) {
        this.inOuts = inOuts;
    }

    public Set<GeneralAssembly> getGeneralAssemblies() {
        return generalAssemblies;
    }

    public void setGeneralAssemblies(Set<GeneralAssembly> generalAssemblies) {
        this.generalAssemblies = generalAssemblies;
    }

    public Set<Sympathisant> getSympathisants() {
        return sympathisants;
    }

    public void setSympathisants(Set<Sympathisant> sympathisants) {
        this.sympathisants = sympathisants;
    }

    public Set<MemberRegistration> getMemberRegistrations() {
        return memberRegistrations;
    }

    public void setMemberRegistrations(Set<MemberRegistration> memberRegistrations) {
        this.memberRegistrations = memberRegistrations;
    }
}
