package com.hz.acrKasseService.cashbox.model.payload;

public class Member {

    private String name;
    private String memberTyp;
    private String intryDate;
    private int amount;
    private int sessionNr;
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMemberTyp() {
        return memberTyp;
    }

    public void setMemberTyp(String memberTyp) {
        this.memberTyp = memberTyp;
    }

    public String getIntryDate() {
        return intryDate;
    }

    public void setIntryDate(String intryDate) {
        this.intryDate = intryDate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSessionNr() {
        return sessionNr;
    }

    public void setSessionNr(int sessionNr) {
        this.sessionNr = sessionNr;
    }
}
