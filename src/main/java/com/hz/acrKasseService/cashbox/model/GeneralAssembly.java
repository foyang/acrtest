package com.hz.acrKasseService.cashbox.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "GeneralAssembly")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class GeneralAssembly {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private int generalAssemblyNummer;

    @Column(nullable = false)
    private String agStartDate;

    @Column(nullable = false)
    private String agEndDate;

    @ManyToMany(mappedBy = "generalAssembly", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<InOutM> inOuts;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "acrSession_id",nullable = false)
    @JsonIgnore
    private AcrSession acrSession;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGeneralAssemblyNummer() {
        return generalAssemblyNummer;
    }

    public void setGeneralAssemblyNummer(int generalAssemblyNummer) {
        this.generalAssemblyNummer = generalAssemblyNummer;
    }


     public Set<InOutM> getInOuts() {
        return inOuts;
    }

    public void setInOuts(Set<InOutM> inOuts) {
        this.inOuts = inOuts;
    }


    public AcrSession getAcrSession() {
        return acrSession;
    }

    public void setAcrSession(AcrSession acrSession) {
        this.acrSession = acrSession;
    }

    public String getAgStartDate() {
        return agStartDate;
    }

    public void setAgStartDate(String agStartDate) {
        this.agStartDate = agStartDate;
    }

    public String getAgEndDate() {
        return agEndDate;
    }

    public void setAgEndDate(String agEndDate) {
        this.agEndDate = agEndDate;
    }
}
