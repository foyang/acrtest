package com.hz.acrKasseService.cashbox.service;

import com.hz.acrKasseService.cashbox.dao.DbUploadDocumentDao;
import com.hz.acrKasseService.cashbox.model.DbUploadDocument;
import com.hz.acrKasseService.exception.FileStorageException;
import com.hz.acrKasseService.exception.MyFileNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

@Service
public class DBFileStorageService {
    @Autowired
    private DbUploadDocumentDao dbUploadDocumentDao;
    private static final Logger logger = LoggerFactory.getLogger(DBFileStorageService.class);

    public DbUploadDocument storeFile(MultipartFile file) {

        if (file != null){
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());

            try {
                // Check if the file's name contains invalid characters
                if(fileName.contains("..")) {
                    throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
                }

                DbUploadDocument dbFile = new DbUploadDocument(fileName, file.getContentType(), file.getBytes());

                return dbUploadDocumentDao.save(dbFile);
            } catch (IOException ex) {
                throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
            }
        }
        // Normalize file name
       return null;
    }

    public DbUploadDocument getFile(String fileId) {
        return dbUploadDocumentDao.findById(fileId)
                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
    }

    public byte[] recoverImageFromUrl(String urlText) throws Exception {
        URL url = new URL(urlText);
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        try (InputStream inputStream = url.openStream()) {
            int n = 0;
            byte [] buffer = new byte[ 1024 ];
            while (-1 != (n = inputStream.read(buffer))) {
                output.write(buffer, 0, n);
            }
        }

        return output.toByteArray();
    }
}
