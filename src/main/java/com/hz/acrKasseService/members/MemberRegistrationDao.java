package com.hz.acrKasseService.members;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MemberRegistrationDao extends JpaRepository<MemberRegistration,Integer> {
    List<MemberRegistration> findByLastName(String lastName);
    MemberRegistration findByFirstName(String lastName);

    List<MemberRegistration> findByEmail(String email);
}
