package com.hz.acrKasseService.members;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hz.acrKasseService.cashbox.model.AcrSession;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "MemberRegistration")
public class MemberRegistration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String profesion;

    @Column(nullable = false)
    private String gender;

    @Column(nullable = false)
    private String adress;

    @Column(nullable = false)
    private int postCode;

    @Column(nullable = false)
    private String place;

    @Email
    private String email;

    @Column(nullable = true)
    private long number;

    @Column(nullable = false)
    private int amount;

    @Column( length = 100000 )
    private String message;

    private Boolean acceptCondition;

    private String registrationDate;

    private String paymentMethod;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "acrSession_id",nullable = false)
    @JsonIgnore
    private AcrSession acrSession;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getAcceptCondition() {
        return acceptCondition;
    }

    public void setAcceptCondition(Boolean acceptCondition) {
        this.acceptCondition = acceptCondition;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public AcrSession getAcrSession() {
        return acrSession;
    }

    public void setAcrSession(AcrSession acrSession) {
        this.acrSession = acrSession;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
