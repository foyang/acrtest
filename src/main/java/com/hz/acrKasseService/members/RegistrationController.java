package com.hz.acrKasseService.members;

import com.hz.acrKasseService.cashbox.dao.AcrCahsCodeDao;
import com.hz.acrKasseService.cashbox.dao.AcrSessionDao;
import com.hz.acrKasseService.cashbox.dao.InOutDao;
import com.hz.acrKasseService.cashbox.model.AcrSession;
import com.hz.acrKasseService.helper.ServiceHelper;
import com.hz.acrKasseService.mail.EmailService;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@Api(description = "This service allows you to record a new Member of the association.")
@RestController
@RequestMapping(value = "public")
public class RegistrationController {

    @Autowired
    private EmailService emailService;

    @Autowired
    private MemberRegistrationDao memberRegistrationDao;

    @Autowired
    private InOutDao inOutDao;

    @Autowired
    private AcrCahsCodeDao acrCahsCodeDao;

    @Autowired
    private AcrSessionDao acrSessionDao;

    private ServiceHelper serviceHelper = new ServiceHelper();

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private Date currentDate = new Date(System.currentTimeMillis());
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    String currentDateString = sdf.format(currentDate);


    @ApiOperation(value = "Register a new Member")
    @PostMapping(value = "addmember")
    public ResponseEntity<?> addCode(@Valid @RequestBody MemberRegistration memberRegistration,
                                     HttpServletRequest request
    ) throws IOException, MessagingException, ParseException {
        // MemberRegistration memberByFistName = memberRegistrationDao.findByFirstName(memberRegistration.getFirstName());

        List<MemberRegistration> checkmemberByLastName = memberRegistrationDao.findByLastName(memberRegistration.getLastName());
        List<MemberRegistration> checkBeyEmailList = memberRegistrationDao.findByEmail(memberRegistration.getEmail());


        if (memberRegistration.getEmail() == null) {
            return new ResponseEntity<>(new ResponseMessage("Email  can not be null."),
                    HttpStatus.BAD_REQUEST);
        }

        AcrSession currentSaision = serviceHelper.getCurrentSeesion(acrSessionDao);

        for (MemberRegistration memberRegistration1 : checkBeyEmailList) {
            if (currentSaision.getId() == memberRegistration1.getAcrSession().getId()) {
                if (memberRegistration1.getEmail().equals(memberRegistration.getEmail()))
                    return new ResponseEntity<>(new ResponseMessage("This email address already exists."),
                            HttpStatus.BAD_REQUEST);
            }
        }


        if (checkmemberByLastName.size() != 0 && checkBeyEmailList.size() != 0) {
            for (MemberRegistration memberRegistration1 : checkmemberByLastName) {


                if (currentSaision.getId() == memberRegistration1.getAcrSession().getId()) {
                    if (memberRegistration1.getFirstName().equals(memberRegistration.getFirstName())) {
                        return new ResponseEntity<>(new ResponseMessage("You are already registering as a member for the current session session."),
                                HttpStatus.BAD_REQUEST);
                    } else {
                        if (memberRegistration.getAcceptCondition()) {
                            serviceHelper.memberRegistrationProzess(memberRegistration, currentSaision,acrCahsCodeDao,acrSessionDao,inOutDao,memberRegistrationDao,emailService);
                        } else {
                            return new ResponseEntity<>(new ResponseMessage("Please accept the conditions"),
                                    HttpStatus.NOT_ACCEPTABLE);
                        }
                    }
                } else {
                    if (memberRegistration.getAcceptCondition()) {
                        serviceHelper.memberRegistrationProzess(memberRegistration, currentSaision,acrCahsCodeDao,acrSessionDao,inOutDao,memberRegistrationDao,emailService);
                    } else {
                        return new ResponseEntity<>(new ResponseMessage("Please accept the conditions"),
                                HttpStatus.NOT_ACCEPTABLE);
                    }
                }
            }
        } else {
            if (memberRegistration.getAcceptCondition()) {
                serviceHelper.memberRegistrationProzess(memberRegistration, currentSaision,acrCahsCodeDao,acrSessionDao,inOutDao,memberRegistrationDao,emailService);
            } else {
                return new ResponseEntity<>(new ResponseMessage("Please accept the conditions"),
                        HttpStatus.NOT_ACCEPTABLE);
            }
        }

        return null;
    }


    @ApiOperation(value = "delete Member")
    @DeleteMapping(value = "delete/{id}")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteCode(@Valid @PathVariable("id") int id) {
        MemberRegistration memberRegistration = memberRegistrationDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("The Code with id: " + id + " was not found: "));
        memberRegistrationDao.delete(memberRegistration);
        return new ResponseEntity<>(new ResponseMessage("The Member with id: " + id + " was deleted"), HttpStatus.OK);
    }

    @ApiOperation(value = "Update Member")
    @PutMapping("update/{id}")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN')")
    public ResponseEntity<?> UpdateCode(@Valid @RequestBody MemberRegistration memberRegistration, @PathVariable("id") int id) {
        update(id, memberRegistration.getFirstName(), memberRegistration.getLastName(), memberRegistration.getEmail(),
                memberRegistration.getAdress(), memberRegistration.getGender(), memberRegistration.getMessage(),
                memberRegistration.getNumber(), memberRegistration.getProfesion());
        return new ResponseEntity<>(new ResponseMessage("The Member with id: " + id + " was update successfully"), HttpStatus.OK);
    }

    public MemberRegistration update(int id, String firstName, String lastName, String email, String adress, String gender,
                                     String message, long number, String profesion) {
        MemberRegistration memberRegistration = memberRegistrationDao.findById(id)
                .orElseThrow(() -> new RuntimeException("The Member with id: " + id + " was not found"));
        memberRegistration.setFirstName(firstName);
        memberRegistration.setLastName(lastName);
        memberRegistration.setAdress(adress);
        memberRegistration.setEmail(email);
        memberRegistration.setGender(gender);
        memberRegistration.setMessage(message);
        memberRegistration.setProfesion(profesion);
        memberRegistration.setNumber(number);
        memberRegistrationDao.save(memberRegistration);
        return memberRegistration;
    }

    @ApiOperation(value = "Get all Register Member")
    @GetMapping("list")
    @PreAuthorize("hasRole('PRESIDENT') or hasRole('ADMIN') or hasRole('HELP')")
    public List<MemberRegistration> getAllCodes() {
        List<MemberRegistration> listMembers = memberRegistrationDao.findAll();
        return listMembers;
    }


    @RequestMapping("/pfdtest")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("toPdfTest");
        return modelAndView;
    }
}
