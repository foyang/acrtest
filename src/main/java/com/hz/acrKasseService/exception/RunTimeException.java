package com.hz.acrKasseService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE)
public class RunTimeException extends RuntimeException {
    public RunTimeException(String s) {
        super(s);
    }
}

