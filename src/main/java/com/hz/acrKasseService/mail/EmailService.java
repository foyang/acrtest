package com.hz.acrKasseService.mail;

import com.google.common.io.ByteStreams;
import com.hz.acrKasseService.members.MemberRegistration;
import com.hz.acrKasseService.services.Html2PdfService;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Map;

import org.thymeleaf.context.Context;

@Service
@RequiredArgsConstructor
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;

    private final Html2PdfService pdfService;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String FILEROOT = "src/main/resources/memberPdf/";


    public void sendSimpleMessage(Mail mail, Map<String, Object> mapMembere) throws MessagingException, IOException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        InputStreamResource inputStreamResource = pdfService.generateInvoice(mapMembere);

        String fileName = mapMembere.get("lastName")+"_"+mapMembere.get("firstName")+"_"+mapMembere.get("registrationDate")+".pdf";

        FileSystemResource file = new FileSystemResource(new File(FILEROOT+fileName));

        if (inputStreamResource != null){
            helper.addAttachment(fileName+".pdf", file);
        }
        // helper.addAttachment("logo.png", new ClassPathResource("memorynotfound-logo.png"));

        Context context = new Context();
        context.setVariables(mail.getModel());
        String html = templateEngine.process("email-template", context);

        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());

        emailSender.send(message);
    }
}
