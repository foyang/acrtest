package com.hz.acrKasseService.mail;

import com.hz.acrKasseService.security.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import org.thymeleaf.context.Context;

@Service
public class NotificationService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    public NotificationService(JavaMailSender javaMailSender){
        this.javaMailSender = javaMailSender;
    }

    public void sendRegistrationNotification(User user) throws MailException {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setFrom("kontakt.bookme@gofastonline.com");
        mailMessage.setSubject("Registration success");
        mailMessage.setText("Enregistrement reussi avec success!!");
        javaMailSender.send(mailMessage);
    }

    public void  sendResetPasswordToken(String userEmail, String token){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(userEmail);
        mailMessage.setFrom("kontakt.bookme@gofastonline.com");
        mailMessage.setSubject("Password reset request");
        mailMessage.setText(token);
        javaMailSender.send(mailMessage);
    }
}
