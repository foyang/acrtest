package com.hz.acrKasseService.services;

import com.itextpdf.html2pdf.HtmlConverter;
import groovy.util.logging.Log;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;

import org.thymeleaf.context.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class Html2PdfServiceImpl implements Html2PdfService {

    private final TemplateEngine templateEngine;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String FILEROOT = "src/main/resources/memberPdf/";

    @Override
    public InputStreamResource generateInvoice(Map<String, Object> data) {
        Context context = new Context();
        context.setVariables(data);
        String fileName = data.get("lastName")+"_"+data.get("firstName")+"_"+data.get("registrationDate")+".pdf";

        String html = templateEngine.process("toPdfTest",context);
        try {
            HtmlConverter.convertToPdf(html,new FileOutputStream(FILEROOT+fileName));
            return new InputStreamResource(new FileInputStream(FILEROOT+fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
