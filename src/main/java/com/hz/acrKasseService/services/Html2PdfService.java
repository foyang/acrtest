package com.hz.acrKasseService.services;

import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.util.Map;

public interface Html2PdfService  {

    InputStreamResource generateInvoice(Map<String,Object> data);
}
