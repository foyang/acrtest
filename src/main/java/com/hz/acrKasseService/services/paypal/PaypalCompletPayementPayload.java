package com.hz.acrKasseService.services.paypal;

public class PaypalCompletPayementPayload {

    private String status;
    private String payment;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }
}
