package com.hz.acrKasseService.services.paypal;

public class PaypalCreatePayload {
    private String status;
    private String redirecUrl;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRedirecUrl() {
        return redirecUrl;
    }

    public void setRedirecUrl(String redirecUrl) {
        this.redirecUrl = redirecUrl;
    }
}
