package com.hz.acrKasseService.services.paypal;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hz.acrKasseService.cashbox.dao.AcrCahsCodeDao;
import com.hz.acrKasseService.cashbox.dao.AcrSessionDao;
import com.hz.acrKasseService.cashbox.dao.InOutDao;
import com.hz.acrKasseService.cashbox.model.AcrSession;
import com.hz.acrKasseService.cashbox.model.payload.Member;
import com.hz.acrKasseService.helper.ServiceHelper;
import com.hz.acrKasseService.mail.EmailService;
import com.hz.acrKasseService.members.MemberRegistration;
import com.hz.acrKasseService.members.MemberRegistrationDao;
import com.hz.acrKasseService.security.jwt.response.ResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/paypal")
public class PayPalController {


    private final PayPalClient payPalClient;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EmailService emailService;

    @Autowired
    private MemberRegistrationDao memberRegistrationDao;

    @Autowired
    private InOutDao inOutDao;

    @Autowired
    private AcrCahsCodeDao acrCahsCodeDao;

    @Autowired
    private AcrSessionDao acrSessionDao;

    private ServiceHelper serviceHelper = new ServiceHelper();

    @Autowired
    PayPalController(PayPalClient payPalClient){
        this.payPalClient = payPalClient;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(value = "/make/payment")
    public ResponseEntity<?> makePayment(@RequestBody MemberRegistration memberRegistration, @RequestParam("sum") int sum, @RequestParam("frontBaseUrl") String frontBaseUrl , HttpServletRequest request) throws ParseException, IOException, MessagingException {
         ObjectMapper paypalResponMap = new ObjectMapper();
         PaypalCreatePayload paypalCreatePayload = new PaypalCreatePayload();

        List<MemberRegistration> checkmemberByLastName = memberRegistrationDao.findByLastName(memberRegistration.getLastName());
        List<MemberRegistration> checkBeyEmailList = memberRegistrationDao.findByEmail(memberRegistration.getEmail());

        if (memberRegistration.getEmail() == null) {
            return new ResponseEntity<>(new ResponseMessage("Email  can not be null."),
                    HttpStatus.BAD_REQUEST);
        }

        AcrSession currentSaision = serviceHelper.getCurrentSeesion(acrSessionDao);

        for (MemberRegistration memberRegistration1 : checkBeyEmailList) {
            if (currentSaision.getId() == memberRegistration1.getAcrSession().getId()) {
                if (memberRegistration1.getEmail().equals(memberRegistration.getEmail()))
                    return new ResponseEntity<>(new ResponseMessage("This email address already exists."),
                            HttpStatus.BAD_REQUEST);
            }
        }


        if (checkmemberByLastName.size() != 0 && checkBeyEmailList.size() != 0) {
            for (MemberRegistration memberRegistration1 : checkmemberByLastName) {


                if (currentSaision.getId() == memberRegistration1.getAcrSession().getId()) {
                    if (memberRegistration1.getFirstName().equals(memberRegistration.getFirstName())) {
                        return new ResponseEntity<>(new ResponseMessage("You are already registering as a member for the current session session."),
                                HttpStatus.BAD_REQUEST);
                    } else {
                        if (memberRegistration.getAcceptCondition()) {
                            paypalCreatePayload = paypalResponMap.convertValue(payPalClient.createPayment(sum, frontBaseUrl),PaypalCreatePayload.class);
                            return new ResponseEntity<Object>(paypalCreatePayload,HttpStatus.OK);
                        } else {
                            return new ResponseEntity<>(new ResponseMessage("Please accept the conditions"),
                                    HttpStatus.NOT_ACCEPTABLE);
                        }
                    }
                } else {
                    if (memberRegistration.getAcceptCondition()) {
                        paypalCreatePayload = paypalResponMap.convertValue(payPalClient.createPayment(sum, frontBaseUrl),PaypalCreatePayload.class);
                        return new ResponseEntity<Object>(paypalCreatePayload,HttpStatus.OK);
                    } else {
                        return new ResponseEntity<>(new ResponseMessage("Please accept the conditions"),
                                HttpStatus.NOT_ACCEPTABLE);
                    }
                }
            }
        } else {
            if (memberRegistration.getAcceptCondition()) {
                paypalCreatePayload = paypalResponMap.convertValue(payPalClient.createPayment(sum, frontBaseUrl),PaypalCreatePayload.class);
                return new ResponseEntity<Object>(paypalCreatePayload,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new ResponseMessage("Please accept the conditions"),
                        HttpStatus.NOT_ACCEPTABLE);
            }
        }


        return null;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(value = "/complete/payment")
    public ResponseEntity<?> completePayment(@RequestBody MemberRegistration memberRegistration, HttpServletRequest request, @RequestParam("paymentId") String paymentId, @RequestParam("payerId") String payerId) throws ParseException, IOException, MessagingException {
        log.info("#####: Dans le Complet Payement");
        Map<String, Object> stringObjectMap = payPalClient.completePayment(request);
        AcrSession currentSaision = serviceHelper.getCurrentSeesion(acrSessionDao);

        if (stringObjectMap.get("status").equals("success")){
            log.info("#####: Dans le Complet Payement DANS LE IF ");
                ResponseEntity<ResponseMessage> responseMessageResponseEntity = serviceHelper.memberRegistrationProzess(memberRegistration, currentSaision, acrCahsCodeDao, acrSessionDao, inOutDao, memberRegistrationDao, emailService);
            if (responseMessageResponseEntity != null)
            {
                return new ResponseEntity<>(new ResponseMessage("Your Registration has been done successfully."),
                        HttpStatus.OK);
            }
        }else if (!stringObjectMap.get("status").equals("success")){
            return new ResponseEntity<>(new ResponseMessage("A problem occurred during the payment."),
                    HttpStatus.BAD_REQUEST);
        }
        return null;
    }
}